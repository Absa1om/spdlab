#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <sstream>
#include <queue>
#include <thread>
#include <functional>

using namespace std;

class timer{
	private: 
		double timeOfStart, measuredTime;

	public: 

		timer(){
			timeOfStart = measuredTime = 0;
		}

		void startTimer(){
				timeOfStart = clock();
		}

		void stopTimer(){
				measuredTime = (clock() - timeOfStart) / CLOCKS_PER_SEC;
				timeOfStart = 0;
		}

		double getElapsedTime(){
			return measuredTime;
		}

};

class timeClass{
	
	public:

	timeClass(){
		size = 3;
		params = new double [size];
	}


	timeClass(int size){
		this->size = size;
		params = new double[size];
	}

	void readParams(istream & strm){
		for(int i = 0; i < size; ++i)
			strm >> params[i];
	}

	void print(){
		for(int i = 0; i < size; ++i)
			cout << params[i] << ' ';
		cout << endl;
	}

	const double & getRj()const{
		return params[0];
	}

	const double & getPj()const{
		return params[1];
	}

	const double & getQj()const{
		return params[2];
	}

	void setRj(double a){
		params[0] = a;
	}

	void setPj(double a){
		params[1] = a;
	}

	void setQj(double a){
		params[2] = a;
	}

	bool operator==(const timeClass & x){
		return this == &x;
	}

	private:

	int size;
	double * params;

};

const double calcCmax(vector <timeClass> permutation){	
    double startTime = 0;
	double lastDelivery = 0;
	double lastCompletion = 0; 

	for (timeClass tc : permutation){
		startTime = max(lastCompletion, tc.getRj());
		lastCompletion = startTime + tc.getPj();
		lastDelivery = max(lastCompletion + tc.getQj(), lastDelivery);
	}

	return lastDelivery;
}

void sortR(vector <timeClass> & permutation){
	sort(permutation.begin(), permutation.end(), [](timeClass & x, timeClass & y){	
			return x.getRj() < y.getRj();
		}
	);
}


void sort2opt(vector <timeClass> & permutation){
	for(auto i = permutation.begin(); i != permutation.end(); ++i){
		for(auto j = i+1; j != permutation.end(); ++j){
			
			if(i!=j){
				volatile double c1,c2;
				c1 = calcCmax(permutation);
				swap(*i, *j);
				c2 = calcCmax(permutation);			
				
				if(c2 < c1)
					j = i = permutation.begin();
				else
					swap(*i, *j);
			}	
		}	
	}
}


struct sortMoreByQ{
    bool operator()(const timeClass & x, const timeClass & y){
        return x.getQj() < y.getQj();
    }
};



struct lessByR{
    bool operator()(const timeClass & x, const timeClass & y){
        return x.getRj() > y.getRj();
    }
};



const timeClass & getElemWithMinR(const std::vector <timeClass> & permutation){
	return *min_element(permutation.begin(), permutation.end(),
            [](const timeClass &x, const timeClass &y){ return x.getRj() > y.getRj();});
}

double getMinR(const std::vector <timeClass> & permutation){
	return min_element(permutation.begin(), permutation.end(),
           [](const timeClass &x, const timeClass &y){ return x.getRj() > y.getRj();})->getRj();
}

void schrage(std::vector<timeClass>& permutation) {
    int t = 0;
    std::priority_queue<timeClass, std::vector<timeClass>, lessByR> N;
    std::priority_queue<timeClass, std::vector<timeClass>, sortMoreByQ> Q;

    for(auto v : permutation) {
        N.push(v);
    }

    permutation.erase(permutation.begin(), permutation.end());

    while (!Q.empty() || !N.empty()) {
       
        while (!N.empty() && N.top().getRj() <= t) {
            Q.push(N.top());
            N.pop();
        }
       
        if (Q.empty()) {
            t = N.top().getRj();
            continue;
        }
       
        auto e = Q.top();
        t += e.getPj();
		Q.pop();
        permutation.push_back(e);
	}
}

int prmtS(std::vector<timeClass>& permutation) {
    int t = 0, c_max = 0, q_0 = std::numeric_limits<int>::max();;
    timeClass l, e;
    l.setQj(q_0);
    std::priority_queue<timeClass, std::vector<timeClass>, lessByR> N;
    std::priority_queue<timeClass, std::vector<timeClass>, sortMoreByQ> Q;
    cout << "sss";
    for(auto v : permutation) {
        N.emplace(v);
    }
	cout << "sss";
    while (!Q.empty() || !N.empty()) {
        while (!N.empty() && N.top().getRj() <= t) {
            e = N.top();
            Q.emplace(e);
            N.pop();
            if (e.getQj() > l.getQj()) {
                l.setPj( t - e.getRj());
                t = e.getRj();
                if (l.getPj() > 0)
                    Q.emplace(l);
            }
        }
        if (Q.empty()) {

            t = N.top().getRj();
            continue;
        }
        e = Q.top();
        Q.pop();
        l = e; t += e.getPj(); c_max = std::max(c_max, int(t+e.getQj()));
    }

    return c_max;
}


void getTimeAndCmaxOfAlghorithm(
	void (*func)(vector<timeClass> &),
	vector<timeClass> & permutation,
	pair <double, double> & timeCmaxPair){
	timer x;
	x.startTimer();
	func(permutation);
	x.stopTimer();
	timeCmaxPair = pair<double, double>(x.getElapsedTime(), calcCmax(permutation));
}

 void getTimeAndCmaxOfAlgh(
	std::function <int (vector<timeClass> &)> func,
	vector<timeClass> & permutation,
	pair <double, double> & timeCmaxPair){
	timer x;
	x.startTimer();
	int g = func(permutation);
	x.stopTimer();
	timeCmaxPair = pair<double, double>(x.getElapsedTime(), g);
}


void sortR2opt(vector <timeClass> & permutation){
	sortR(permutation);
	sort2opt(permutation);
}


int main(int argc, char ** argv){
		
	for(int j = 1; j < argc; j++){
	
		ifstream stream;
		int count, size;
				
		stream.open(argv[j]);
		stream >> count >> size;
		vector <timeClass> taskVector;

		for(int i = 0; i < count; i++)
		{	
			timeClass tmp(size);
			tmp.readParams(stream);
			taskVector.push_back(tmp);
		}

		auto vec1 = taskVector, vec2 = taskVector, vec3 = taskVector, vec4 = taskVector;

		pair <double, double> twoOpt, rsort, schragePair, prmtSpair;
//		std::thread t1(getTimeAndCmaxOfAlghorithm, sort2opt, std::ref(vec1), std::ref(twoOpt));
		std::thread t2(getTimeAndCmaxOfAlghorithm, sortR, std::ref(vec2), std::ref(rsort));
		std::thread t3(getTimeAndCmaxOfAlghorithm, schrage, std::ref(vec3), std::ref(schragePair));
		std::thread t4(getTimeAndCmaxOfAlgh, prmtS, std::ref(vec4), std::ref(prmtSpair));

///		t1.join();
		t2.join();
		t3.join();
		t4.join();

		prmtS(vec4);

		cout << "12345 cmax: " << calcCmax(taskVector) <<  endl ;

		cout << "sortR cmax: " << rsort.second << " and time of execution: " << rsort.first<< " " <<  endl ;
//		cout << "sort2opt cmax: " << twoOpt.second << " and time of execution: " << twoOpt.first<< " " <<  endl ; 
		cout << "schrage cmax: " << schragePair.second << " and time of execution: " << schragePair.first << " " << endl;
		cout << "prmt schrage cmax: " << prmtSpair.second << " and time of execution: " << prmtSpair.first << " " << endl;
		cout << "for file " << argv[j] << endl; 

		stream.close();
		stream.clear();
	}

	return 0;
}

