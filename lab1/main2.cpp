#include <iostream>
#include <fstream>
#include <vector>
#include <functional>
#include <queue>
#include <iostream>

struct Process {
    int availability_ {} ;
    int processing_time_ {};
    int delivery_time_ {};
};

struct compare_by_delivery_time
{
    bool operator()(const Process lhs, const Process rhs) const
    {
        return lhs.delivery_time_ < rhs.delivery_time_;
    }
};

struct compare_by_availability
{
    bool operator()(const Process lhs, const Process rhs) const
    {
        return lhs.availability_ > rhs.availability_;
    }
};

void validateStartParams(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Invalid input value, please run program like: " <<argv[0]<<" INPUT.DAT\n";
        std::terminate();
    }
}

template <typename Container>
void printContainer(const Container& processes) {
    for (auto v : processes) {
        std::cout <<v.availability_<<"\t"
                  <<v.processing_time_ <<"\t"
                  <<v.delivery_time_<< "\n";
    }
}

void prepareProcessData(const std::string& filename, std::vector<Process>& proc) {
    std::ifstream file(filename);
    if (file.is_open()) {
        int number_of_processes{};
        file >> number_of_processes;

        for (int i = 0; i < number_of_processes; i++) {
            Process process;
            file >> process.availability_;
            file >> process.processing_time_;
            file >> process.delivery_time_;
            proc.emplace_back(process);
        }
    }
    else {
        std::cerr << "Unable to open '" << filename << "'\n";
        std::terminate();
    }
}

int SchrageAlgorithm(std::vector<Process>& processes) {
    int t = 0, k = 0, c_max = 0;

    std::priority_queue<Process, std::vector<Process>, compare_by_availability> N;
    std::priority_queue<Process, std::vector<Process>, compare_by_delivery_time> Q;

    for(auto v : processes) {
        N.emplace(v);
    }

    while (!Q.empty() || !N.empty()) {
        while (!N.empty() && N.top().availability_ <= t) {
            Q.emplace(N.top());
            N.pop();
        }
        if (Q.empty()) {
            t = N.top().availability_;
            continue;
        }
        auto e = Q.top();
        Q.pop();
        k++; t += e.processing_time_;c_max = std::max(c_max, t+e.delivery_time_);
    }
    return c_max;
}

int main(int argc, char** argv) {
    validateStartParams(argc, argv);

    std::string filename = argv[1];
    std::vector<Process> processes;

    prepareProcessData(filename, processes);

    std::cout<<SchrageAlgorithm(processes);


    return 0;
}