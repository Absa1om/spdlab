#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <sstream>
#include <queue>
#include <thread>
#include <functional>

using namespace std;

class timer{
	private: 
		double timeOfStart, measuredTime;

	public: 

		timer(){
			timeOfStart = measuredTime = 0;
		}

		void startTimer(){
				timeOfStart = clock();
		}

		void stopTimer(){
				measuredTime = (clock() - timeOfStart) / CLOCKS_PER_SEC;
				timeOfStart = 0;
		}

		double getElapsedTime(){
			return measuredTime;
		}

};

class timeClass{
	
	public:

	timeClass(){
		size = 3;
		params = new double [size];
		delay = 0;
		completiontime = 0;
	}


	timeClass(int size){
		this->size = size;
		params = new double[size];
		delay = 0;
		completiontime = 0;
	}

	timeClass(const timeClass & x){
		for(int i = 0; i < 3 ; i++){
			this.params[i] = x.params[i];
		}
		delay = 0;
		completiontime = 0;
	}

	void readParams(istream & strm){
		for(int i = 0; i < size; ++i)
			strm >> params[i];
	}

	void print(){
		for(int i = 0; i < size; ++i)
			cout << params[i] << ' ';
		cout << endl;
	}

	const double & getPj()const{
		return params[0];
	}

	const double & getWj()const{
		return params[1];
	}

	const double & getDj()const{
		return params[2];
	}

	void setPj(double a){
		params[0] = a;
	}

	void setWj(double a){
		params[1] = a;
	}

	void setDj(double a){
		params[2] = a;
	}

	bool operator==(const timeClass & x){
		return this == &x;
	}

	private:

	int size;
	double * params;
	public: 
	int delay;
	int completiontime;
}; 

void sortByDeadline(vector <timeClass> & permutation){
	sort(permuation.begin(), permuation.end(),
		[](const timeClass & x, const timeClass & y)
			{ return  x.getDj() > y.getDj();}
	);
}



void calcCompletion(vector <timeClass> & permutation){
	int lastCompletion = permutation[0].getPj();
	permuation[0].completiontime = lastCompletion;

	for(int i = 1; i < permuation.size(); i++){
		lastCompletion = permutation[i].getDj() + lastCompletion;
		permutation[i].completiontime = lastCompletion;
	}

}

void calcDelay(vector <timeClass> & permutation){
	for(auto x : permuation){
		x.delay = max( x.completiontime - x.getDj, 0);
	}
}
 
int calcWiTi(vector <timeClass> & permutation){
	int witi = 0;

	for(int i = 0; i< permuation.size ;i++){
		witi += permuation[i].getWj() * permuation[i].delay;
	}

}


int main(int argc, char ** argv){
		
	for(int j = 1; j < argc; j++){
	
		ifstream stream;
		int count, size;
				
		stream.open(argv[j]);
		stream >> count >> size;
		vector <timeClass> taskVector;

		for(int i = 0; i < count; i++)
		{	
			timeClass tmp(size);
			tmp.readParams(stream);
			taskVector.push_back(tmp);
		}

		calcCompletion(taskVector);
		calcDelay(taskVector);



	}

	return 0;
}

