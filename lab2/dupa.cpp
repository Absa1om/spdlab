#include <iostream>
#include <vector>
#include <map>
#include <iomanip>
#include <algorithm>

#define GETBIT(number,bit) (number) & 1<<(bit)
#define CLEARBIT(number,bit) (number) & ~(1<<(bit))

using namespace std;

struct PWD
{
    int p, w, d;
};

istream &operator >> (istream &is, PWD &pwd)
{
    is >> pwd.p >> pwd.w >> pwd.d;
    return is;
}

class WiTi
{
    vector<PWD> data;
    int n;

public:
    void load()
    {
        PWD pwd;
        cin >> n;
        data.clear();

        for(int i=0; i<n; i++)
        {
            cin >> pwd;
            data.push_back(pwd);
        }
    }

    int _solve(int *witi, int binary)
    {
        // cout << setbase(2) << binary << endl;
        int min = INT_MAX;
        int p = 0; // czas wykonywania zadan (z binary)

        if(witi[binary]>=0)
            return witi[binary];

        for(int i=0; i<n; i++)
        {
            // cout << i << endl;
            if(GETBIT(binary, i))
                p += data[i].p;
        }

        for(int i=0; i<n; i++)
        {
            // cout << i << endl;
            if(GETBIT(binary, i))
            {
                int tmp = _solve(witi, CLEARBIT(binary, i)); // wywolaj siebie z jednym zadaniem mniej

                if(p > data[i].d) // jesli przekroczono termin dostawy
                    tmp += (p - data[i].d) * data[i].w; // dolicz wartosc kary zabranego zadania
                if(tmp < min)
                    min = tmp; // znaleziono mniejsze
            }
        }
        witi[binary] = min; // zapamietaj wynik
        return min;
    }

    void solve()
    {
        int *witi = new int[1<<n];

        witi[0] = 0; //uszeregowanie 0 zadan nie wprowadzi zadnych kar
        for(int i=1; i<(1<<n); i++)
        {
            witi[i] = -1; //jesli jest nieobliczone to ma wartosc -1, kary sa tylko dodatnie
        }
        cout << _solve(witi, (1<<n)-1); // (1<<n)-1 = 1111111111111...(binarnie)
    }

    void sort(){
        std::sort(data.begin(), data.end(), []( const PWD& pwd1, const PWD& pwd2 ) {
            return pwd1.d > pwd2.d;
        });
    }

};

int main()
{
    WiTi witi;
    witi.load();
    witi.solve();
}
