#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <time.h>
#include <sstream>
#include <queue>
#include <thread>
#include <functional>
#include <map>
#include <iomanip>

using namespace std;

class timer{
	private:
		double timeOfStart, measuredTime;

	public: 

		timer(){
			timeOfStart = measuredTime = 0;
		}

		void startTimer(){
				timeOfStart = clock();
		}

		void stopTimer(){
				measuredTime = (clock() - timeOfStart) / CLOCKS_PER_SEC;
				timeOfStart = 0;
		}

		double getElapsedTime(){
			return measuredTime;
		}

};

class timeClass{
	
	public:

	timeClass(){
		size = 5;
		params = new double [size];
        params[3] = params  [4] = 0;
        }

	void readParams(istream & strm){
		for(int i = 0; i < 3; ++i)
			strm >> params[i];
        
	}

	void print(){
		for(int i = 0; i < size; ++i)
			cout << params[i] << ' ';
		cout << endl;
	}

	const double & getPj()const{
		return params[0];
	}

	const double & getWj()const{
		return params[1];
	}

	const double & getDj()const{
		return params[2];
	}

	void setPj(double a){
		params[0] = a;
	}

	void setWj(double a){
		params[1] = a;
	}

	void setDj(double a){
		params[2] = a;
	}

    double & Cj(){
        return params[3];
    }

    double & getDelay(){
        return params[4];
    }

	bool operator==(const timeClass & x){
		return this == &x;
	}

	private:

	int size;
	double * params;

};

using Perm = vector<timeClass>;

void calcCj(Perm & permutation){
    double lastCompletion = permutation.begin()->Cj() = permutation.begin()->getPj();
    for (auto i = permutation.begin() + 1; i != permutation.end(); i++) {
        i->Cj() = lastCompletion + i->getPj();
        lastCompletion = i->Cj();
    }
}   

void calcDelay(Perm & permutation){
    for(auto i = permutation.begin(); i != permutation.end(); i++){
        double delay = i->Cj() - i->getDj();
        i->getDelay() = max(delay, 0.0);
    }
}

double calcWiti(Perm & permutation){
    calcCj(permutation);
    calcDelay(permutation);

    double witi = 0; 
    for(auto x : permutation){
        witi += x.getWj() * x.getDelay();
    }
    return witi;
}

void sortByD(Perm & permutation){
    sort(permutation.begin(), permutation.end(),
        [](const timeClass & lhs, const timeClass & rhs){
            return lhs.getDj() < rhs.getDj();
        });
}

pair <double, double> getTimeAndCmaxOfAlghorithm(
	void (*func)(vector<timeClass> &),
	vector<timeClass> & permutation){
	timer x;
	x.startTimer();
	func(permutation);
	x.stopTimer();
	return pair<double, double>(x.getElapsedTime(), calcWiti(permutation));
}

pair <double, double> getTimeAndCmaxOfAlghorithm(
	std::function <int (vector<timeClass> &)> func,
	vector<timeClass> & permutation){
	timer x;
	x.startTimer();
	int g = func(permutation);
	x.stopTimer();
	return pair<double, double>(x.getElapsedTime(), g);
}


int dynamicProgramingAlg(int *witi, int binary, Perm & data)
    {
		int n = data.size();
        int min = INT_MAX;
        int p = 0; 

        if(witi[binary]>=0)
            return witi[binary];

        for(int i=0; i<n; i++)
        {
            if(binary & 1<< i)
                p += data[i].getPj();
        }

        for(int i=0; i<n; i++)
        {
            if(binary & 1<< i)
            {
                int tmp = dynamicProgramingAlg(witi, CLEARBIT(binary, i), data); 

                if(p > data[i].getDj())
                    tmp += (p - data[i].getDj()) * data[i].getWj();
                if(tmp < min)
                    min = tmp;
            }
        }
        witi[binary] = min; 
        return min;
    }

    int dynamicProgramingAlg(Perm & permutation)
    {
		int n = permutation.size();
        
		int *witi = new int[1<<n];

        witi[0] = 0;
        
		for(int i=1; i<(1<<n); i++)
        {
            witi[i] = -1; 
        }
        
		return dynamicProgramingAlg(witi, (1<<n)-1, permutation); 
		
		delete [] witi;
    }



int main(int argc, char ** argv){
		
	for(int j = 1; j < argc; j++){
	
		ifstream stream;
		int count, size;
				
		stream.open(argv[j]);
		stream >> count ;
		vector <timeClass> taskVector;

		for(int i = 0; i < count; i++)
		{	
			timeClass tmp;
			tmp.readParams(stream);
			taskVector.push_back(tmp);
		}
		
		Perm vec1 = taskVector;
		Perm vec2 = taskVector;

		auto sortd = getTimeAndCmaxOfAlghorithm(sortByD, vec1);	
		auto dynamic = getTimeAndCmaxOfAlghorithm(dynamicProgramingAlg, vec2);
		
		std::cout << "witi dla 12345 " << calcWiti(taskVector) << endl;
		std::cout << "witi dla dynamic Programing : " << dynamic.second << endl << "Algorytm wykonywal sie " <<dynamic.first << " sekund \n";
		std::cout << "witi dla sortD : " << sortd.second << endl << "Algorytm wykonywal sie " <<sortd.first << " sekund \n";		
		std::cout << endl << endl;
		stream.close();
		stream.clear();
	}

	return 0;
}