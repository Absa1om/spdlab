#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <time.h>
#include <unistd.h>
#include <list>
#include <fstream>

using namespace std;

void sortNEH(std::vector< std::vector< int>> & vector)
{
    std::stable_sort(vector.begin(), vector.end(), [](auto a, auto b)
    {
        return std::accumulate(a.begin(), a.end(), 0) > std::accumulate(b.begin(), b.end(), 0);
    });
}


int nehAlgorithm(std::vector< std::vector< int>> & data)
{
    int Cmax = 0;
    sortNEH(data);

    int numberOfMachines = data.at(0).size();

    std::vector<std::vector<int>> perm;
    std::vector<std::vector<int>> bestPerm;

    perm.push_back(data.at(0));

    for (int i = 1; i < data.size(); i++)
    {

        if (i != 1)
            perm = bestPerm;

        perm.push_back(data.at(i));
        int theBest = 9999999; 

        for (int j = perm.size() - 1; j >= 0; j--)
        {
            int numberOfJobs =  perm.size();

            std::vector< std::vector<int>> tmpTab = std::vector<std::vector<int>>(numberOfJobs, std::vector<int> (numberOfMachines) );
            
            for (int job = 0; job < numberOfJobs; job++) 
            {
                for (int machine = 0; machine < numberOfMachines; machine++)
                {
                    if (machine == 0 && job == 0)
                    {
                        tmpTab[job][machine] = perm.at(job).at(machine);
                    }
                    else if (machine == 0)
                    {
                        tmpTab[job][machine] = std::max(tmpTab[job-1][machine], 0) + perm.at(job).at(machine);
                    }
                    else if (job == 0)
                    {
                        tmpTab[job][machine] = std::max(tmpTab[job][machine-1], 0) + perm.at(job).at(machine);
                    }
                    else
                    {
                        tmpTab[job][machine] = std::max(tmpTab[job-1][machine], tmpTab[job][machine-1]) + perm.at(job).at(machine);
                    }
                }
            }

            if (tmpTab[numberOfJobs - 1][numberOfMachines - 1] <= theBest) 
            {
                theBest = tmpTab[numberOfJobs - 1][numberOfMachines - 1];
                bestPerm = perm;
                Cmax = theBest;
            }

            if ( j != 0 )
            {
                std::swap(perm.at(j), perm.at(j - 1));
            }
        }
    }
    
    for(auto x : bestPerm){
        for(auto y : x){
            std::cout<< y << " ";
        }
    }
    return Cmax;


}





int main(int argc, char ** argv){
    int rowSize = 0, numOfMachines = 0;

    for(int i = 1; i < argc; i++){
        std::vector <std::vector <int>> data;
        std::ifstream file;
        int tmp;
        file.open(argv[i]);

        file >> rowSize >> numOfMachines;

        data = std::vector< std::vector<int>> (rowSize, std::vector<int>(numOfMachines));

        for(int j = 0; j < rowSize; j++)
        {
            for(int k = 0; k < numOfMachines; k++){
                file >> tmp >> data[j][k];
            }
        }

        for(auto x : data){
            for(auto y : x){
                std::cout<< y << " ";
            }
    }
        
        std::cout << nehAlgorithm(data);

    }
    
    return 0;
}

