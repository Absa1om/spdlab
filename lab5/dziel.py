
import os
 
global name_input_file
name_input_file = input ("File name: ")                 # wczytaj nazwe pliku
global input_file_handler                               # utworz handler pliku wejsciowego
input_file_handler = open(name_input_file, 'r')         #
 
global name_output_file                                 # nazwa pliku wyjscia
name_output_file = "start"                              #
global output_file_handler                              # handler pliku wyjscia
output_file_handler = open(name_output_file, 'w')
 
 
for line in input_file_handler:                             # dla kazdej lini pliku wejsciowego                                
    if len(line.strip()) == 0:                              # jezeli wykryjesz pusta linie
        output_file_handler.close()                         # zamknij poprzedni plik
        name_output_file = input_file_handler.readline()    # przeczytaj nastepna linie z nazwa
        name_output_file = name_output_file[:-1]            # usun ostatni znak bialy wyrazu
        name_output_file = name_output_file+".txt"          # dodaj rozszerzenie tekstowe
        if len(name_output_file.strip()) == 0:              # jezeli nic nie masz, równoważne z pustą linia .strip usuwa potworzone znaki
            break                                           # przerwij pętle
        output_file_handler = open(name_output_file, 'w')   # utworz handler pliku wyjsciowego
    if len(line.strip()) != 0:                              # jezeli linia nie jest linia pusta
         output_file_handler.write(line)                    # przepisz linie
 
os.remove("start")                                          # usun stworzony na poczatku plik start na potrzeby zmiennej globalnej
print("Done")                                               # napisz "done" kiedy skonczysz