#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <time.h>
#include <unistd.h>
#include <list>
#include <fstream>

using namespace std;



class timer{
	private: 
		double timeOfStart, measuredTime;

	public: 

		timer(){
			timeOfStart = measuredTime = 0;
		}

		void startTimer(){
				timeOfStart = clock();
		}

		void stopTimer(){
				measuredTime = (clock() - timeOfStart) / CLOCKS_PER_SEC;
				timeOfStart = 0;
		}

		double getElapsedTime(){
			return measuredTime;
		}

};


void saSwap(int x, int y,std::vector< std::vector< int>> & vector ){
    std::swap(vector[x],vector[y]);
}


void saTwist(int x, int y,std::vector< std::vector< int>> & vector ){
    if(y>x) {
            for (int i = 0; i < (y - x - 1); i++) {
                swap(vector[x + i], vector[y - i]);
            }
        }else{
            for(int i=0;i<(x - y - 1);i++) {        
                swap(vector[y + i], vector[x - i]);
            }
        }
}

void saInsert(int x, int y,std::vector< std::vector< int>> & vector ){
        std::vector< int > vector2 = vector[y];
        vector.erase(vector.begin() + y);
        vector.insert(vector.begin() + x, vector2);
}


void sortNEH(std::vector< std::vector< int>> & vector)
{
    std::stable_sort(vector.begin(), vector.end(), [](auto a, auto b)
    {
        return std::accumulate(a.begin(), a.end(), 0) > std::accumulate(b.begin(), b.end(), 0);
    });
}


int cmax(std::vector< std::vector< int>> & vector){            
    int numberOfMachines = vector.at(0).size();
    int numberOfJobs = vector.size();
    std::vector< std::vector<int>> tmpTab = std::vector<std::vector<int>>(numberOfJobs, std::vector<int> (numberOfMachines) );
    
    for (int job = 0; job < numberOfJobs; job++) 
    {
        for (int machine = 0; machine < numberOfMachines; machine++)
        {
            if (machine == 0 && job == 0)
            {
                tmpTab[job][machine] = vector.at(job).at(machine);
            }
            else if (machine == 0)
            {
                tmpTab[job][machine] = std::max(tmpTab[job-1][machine], 0) + vector.at(job).at(machine);
            }
            else if (job == 0)
            {
                tmpTab[job][machine] = std::max(tmpTab[job][machine-1], 0) + vector.at(job).at(machine);
            }
            else
            {
                tmpTab[job][machine] = std::max(tmpTab[job-1][machine], tmpTab[job][machine-1]) + vector.at(job).at(machine);
            }
        }
    }
    return tmpTab[numberOfJobs - 1][numberOfMachines - 1];

}

int nehAlgorithm(std::vector< std::vector< int>> & data)
{
    int Cmax = 0;

    sortNEH(data);

    int numberOfMachines = data.at(0).size();

    std::vector<std::vector<int>> perm;
    std::vector<std::vector<int>> bestPerm;

    perm.push_back(data.at(0));

    for (int i = 1; i < data.size(); i++)
    {

        if (i != 1)
            perm = bestPerm;

        perm.push_back(data.at(i));
        int theBest = 9999999; 

        for (int j = perm.size() - 1; j >= 0; j--)
        {
            int numberOfJobs =  perm.size();

            std::vector< std::vector<int>> tmpTab = std::vector<std::vector<int>>(numberOfJobs, std::vector<int> (numberOfMachines) );
            
            for (int job = 0; job < numberOfJobs; job++) 
            {
                for (int machine = 0; machine < numberOfMachines; machine++)
                {
                    if (machine == 0 && job == 0)
                    {
                        tmpTab[job][machine] = perm.at(job).at(machine);
                    }
                    else if (machine == 0)
                    {
                        tmpTab[job][machine] = std::max(tmpTab[job-1][machine], 0) + perm.at(job).at(machine);
                    }
                    else if (job == 0)
                    {
                        tmpTab[job][machine] = std::max(tmpTab[job][machine-1], 0) + perm.at(job).at(machine);
                    }
                    else
                    {
                        tmpTab[job][machine] = std::max(tmpTab[job-1][machine], tmpTab[job][machine-1]) + perm.at(job).at(machine);
                    }
                }
            }

            if (tmpTab[numberOfJobs - 1][numberOfMachines - 1] <= theBest) 
            {
                theBest = tmpTab[numberOfJobs - 1][numberOfMachines - 1];
                bestPerm = perm;
                Cmax = theBest;
            }

            if ( j != 0 )
            {
                std::swap(perm.at(j), perm.at(j - 1));
            }
        }
    }
    
    return Cmax;


}




int simulatedAnnealingAlg(std::vector< std::vector< int>> & data, float temp, float wspo){

    srand((int)time( NULL ));
    int i = 0;
    std::vector< std::vector< int>> & bestPerm = data;
    int bestCmax = 99999999;
    int x = 0, y = 0;
    int oldF, currentF;
    float T = temp;
    float wsp = wspo;

    float random = 0.0;
    float deltaF = 0.0f;
    float probability = 0.0f ;

    while(T > 0.0000000001) { 

        oldF = cmax(data);
        if(oldF < bestCmax){
            bestCmax = currentF;
            bestPerm = data;
        }
        x = (std::rand() % data.size());
        
        do {
            y = (std::rand() % data.size());
        } while (x == y);

        int rnd_motion = ( std::rand() % 3);        

        switch (rnd_motion){
            case 0:
                saSwap(x, y, data);
                break;
            case 1:
                saTwist(x, y, data);
                break;
            case 2:
                saInsert(x, y, data);
                break;

        }

        currentF = cmax(data);
        
        if(oldF < currentF){
            deltaF = oldF - currentF;
            random = ( std::rand() % 100)/100.f;
            probability = exp(deltaF/T);
            if(random > probability){
                
                switch (rnd_motion){
                    case 0:
                        saSwap(x, y, data);
                        break;
                    case 1:
                        saTwist(y, x, data);
                        break;
                    case 2:
                        saInsert(y, x, data);
                        break;
                }
            }
        }



        if(T>0)
            T = T * wsp;
        else 
            break;
        i++;
    }

    data = bestPerm;
    return cmax(data);

}

int main(int argc, char ** argv){
    int rowSize = 0, numOfMachines = 0;

    for(int i = 1; i < argc; i++){
        std::vector <std::vector <int>> data;
        std::ifstream file;
        int tmp;
        file.open(argv[i]);

        file >> rowSize >> numOfMachines;

        data = std::vector< std::vector<int>> (rowSize, std::vector<int>(numOfMachines));

        for(int j = 0; j < rowSize; j++)
        {
            for(int k = 0; k < numOfMachines; k++){
                file >> tmp >> data[j][k];

            }
        }
        std::vector <std::vector <int>> data1 = data;



        timer x;
	    x.startTimer();
        std::cout << " SA cmax " << simulatedAnnealingAlg(data1, 100, 0.99)  << "\n";
        x.stopTimer();
        std::cout << " SA time " << x.getElapsedTime()  << "\n";

        timer y;
	    y.startTimer();
        std::cout << " NEH cmax: " << nehAlgorithm(data) << "\n";
        y.stopTimer();
        std::cout << " NEH time " << y.getElapsedTime()  << "\n";

        cout << " dla pliku " << argv[i] << "\n";
        
        }
    
    return 0;
}

